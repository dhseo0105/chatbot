import re
import urllib.request
import http
import sys
from bs4 import BeautifulSoup
from urllib import parse

def get_meaning_of_word(word):
    if not "music" in word:
        return "`@<봇이름> music` 과 같이 멘션해주세요."

    text = input()
    text = parse.quote(text)

    if text.isspace():
        sys.exit


    url = 'https://wordrow.kr/%EC%9D%98%EB%AF%B8/'+ text


    try:
        sourcecode = urllib.request.urlopen(url).read()

    except http.client.IncompleteRead as e:
        sourcecode = e.partial


    sop = BeautifulSoup(sourcecode, "html.parser")
    word_list = sop.find("h3",class_="card-caption").get_text()

    print(word_list)


    return 1

get_meaning_of_word("music")